import 'package:flutter/material.dart';
import 'package:perizinan_keluar_sekolah/konfigurasi_poin/konfigurasi_batas_poin.dart';
import 'package:perizinan_keluar_sekolah/master_poin/home_master_poin.dart';
import 'package:perizinan_keluar_sekolah/siswa/home_poin_siswa.dart';
import 'package:perizinan_keluar_sekolah/satpam/home_poin_satpam.dart';
import 'package:perizinan_keluar_sekolah/gurupiket/home_transaksi_poin.dart';
import 'package:perizinan_keluar_sekolah/verifikasi_poin/home_verifikasi_poin.dart';

class MainMenu extends StatelessWidget {
  const MainMenu({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Perizinan Keluar Sekolah'),
      ),
      body: ListView(
        padding: const EdgeInsets.only(
          left: 5,
          top: 20,
          right: 5,
        ),
        children: [
          ElevatedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const HomeMasterPoin(),
                ),
              );
            },
            child: const Text('Master Poin'),
          ),
          const SizedBox(
            height: 15,
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const HomeTransaksiPoin(),
                ),
              );
            },
            child: const Text('Guru Piket'),
          ),
          const SizedBox(
            height: 15,
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const HomeVerifikasiPoin(),
                ),
              );
            },
            child: const Text('Verifikasi Poin'),
          ),
          const SizedBox(
            height: 15,
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const HomePoinSiswa(),
                ),
              );
            },
            child: const Text('Siswa'),
          ),
          const SizedBox(
            height: 15,
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const HomePoinSatpam(),
                ),
              );
            },
            child: const Text('Satpam'),
          ),
          const SizedBox(
            height: 15,
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const KonfigurasiBatasPoin(),
                ),
              );
            },
            child: const Text('Konfigurasi'),
          ),
        ],
      ),
    );
  }
}
