import 'package:flutter/material.dart';
import 'package:searchfield/searchfield.dart';

class AddTransaksiPoin extends StatefulWidget {
  const AddTransaksiPoin({super.key});

  @override
  State<AddTransaksiPoin> createState() => _AddTransaksiPoinState();
}

class _AddTransaksiPoinState extends State<AddTransaksiPoin> {
  User? selectedUser;
  List<User> users = [
    User("Muhammad Wildhan"),
    User("Sofyan Egi"),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back),
        ),
        backgroundColor: const Color(0xff53A4F5),
        title: const Text(
          "Tambah Transaksi Poin",
          style: TextStyle(
            fontSize: 14,
          ),
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              'NIPD *',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              decoration: BoxDecoration(
                color: const Color(0xffE7F2FF),
                borderRadius: BorderRadius.circular(12),
              ),
              child: Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: SearchField<User>(
                  hint: 'Ketikkan NIPD atau nama siswa',
                  suggestions: users
                      .map(
                        (e) => SearchFieldListItem<User>(
                          e.username,
                          item: e,
                        ),
                      )
                      .toList(),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              'Poin *',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              decoration: BoxDecoration(
                color: const Color(0xffE7F2FF),
                borderRadius: BorderRadius.circular(12),
              ),
              child: Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: SearchField<User>(
                  hint: 'Cari kode / nama poin',
                  suggestions: users
                      .map(
                        (e) => SearchFieldListItem<User>(
                          e.username,
                          item: e,
                        ),
                      )
                      .toList(),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              'Keterangan *',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 120,
              decoration: BoxDecoration(
                color: const Color(0xffE7F2FF),
                borderRadius: BorderRadius.circular(12),
              ),
              child: TextField(
                maxLines: 5,
                style: const TextStyle(fontSize: 14),
                onTap: () {},
                decoration: const InputDecoration(
                    hintText:
                        'Contoh : Melakukan sesuatu di belakang sekolah bersama sekelompok temannya',
                    hintStyle: TextStyle(fontSize: 14),
                    alignLabelWithHint: true,
                    contentPadding: EdgeInsets.only(
                      top: 10,
                      left: 10,
                      right: 15,
                    ),
                    border: InputBorder.none),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              'Bukti Foto/Rekaman',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 10,
            ),
            InkWell(
              onTap: () {},
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 50,
                decoration: BoxDecoration(
                  color: const Color(0xffE7F2FF),
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: Row(
                    children: [
                      Container(
                        width: 90,
                        height: 30,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(color: Colors.black, width: 0.5),
                        ),
                        child: const Center(
                          child: Text(
                            'Choose File',
                            style: TextStyle(
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      const Text(
                        'No file chosen',
                        style: TextStyle(color: Color(0xff7E7E7E)),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              height: 22,
              decoration: BoxDecoration(
                color: const Color(0xff95C5FF),
                borderRadius: BorderRadius.circular(16),
              ),
              child: const Padding(
                padding: EdgeInsets.all(4.5),
                child: Text(
                  'Format file yang diterima: .png, .jpg, .jpeg, .mp4, .m4v, .mkv',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 10,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            const Text("*) Harus diisi"),
            const SizedBox(
              height: 35,
            ),
            InkWell(
              onTap: () {},
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 38,
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.circular(16),
                ),
                child: const Center(
                  child: Text(
                    'Tambah',
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.w600),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class User {
  final String username;
  User(this.username);
}
