import 'package:flutter/material.dart';
import 'package:perizinan_keluar_sekolah/gurupiket/card_transaksi_poin.dart';

class HomeTransaksiPoin extends StatelessWidget {
  const HomeTransaksiPoin({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 255, 255, 1),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
        ),
        title: const Text(
          "Verifikasi Perizinan Siswa",
          style: TextStyle(
            fontSize: 20,
            color: Colors.black,
          ),
        ),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(50),
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Container(
              height: 36,
              padding: const EdgeInsets.symmetric(
                horizontal: 15,
              ),
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 56, 135, 199),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Stack(
                children: [
                  const Positioned(
                    top: 0,
                    bottom: 0,
                    right: 0,
                    child: Icon(
                      Icons.search,
                      color: Color.fromARGB(255, 255, 255, 255),
                    ),
                  ),
                  TextField(
                    style: const TextStyle(fontSize: 14),
                    onTap: () {},
                    decoration: const InputDecoration(
                        hintText: 'Search...',
                        hintStyle: TextStyle(fontSize: 14, color: Colors.white),
                        contentPadding: EdgeInsets.only(
                          top: 11,
                          bottom: 7,
                          left: 0,
                          right: 15,
                        ),
                        isDense: true,
                        border: InputBorder.none),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      body: ListView(
        padding: const EdgeInsets.all(15),
        children: const [
          CardTransaksiPoin(
            nipd: '123',
            keperluan: 'Ke dokter gigi',
            namaSiswa: 'Renal Mutaqin',
            kelas: 'XI MIPA 6',
            lamakeluar: '120 Menit',
          ),
          SizedBox(
            height: 20,
          ),
          CardTransaksiPoin(
            nipd: '193040025',
            keperluan: 'S-001',
            namaSiswa: 'Sofyan Egi Lesmana',
            kelas: '2022-10-03 07:45:43',
            lamakeluar: '120 Menit',
          ),
          SizedBox(
            height: 20,
          ),
          CardTransaksiPoin(
            nipd: '193040002',
            keperluan: 'P-002',
            namaSiswa: 'David Dalil Tauhid',
            kelas: '2022-09-01 07:45:43',
            lamakeluar: '50',
          ),
        ],
      ),
    );
  }
}
