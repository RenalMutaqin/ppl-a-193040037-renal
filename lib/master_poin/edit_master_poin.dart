import 'package:flutter/material.dart';

class EditMasterPoin extends StatefulWidget {
  const EditMasterPoin({super.key});

  @override
  State<EditMasterPoin> createState() => _EditMasterPoinState();
}

class _EditMasterPoinState extends State<EditMasterPoin> {
  Poin? selectedPoin;
  List<Poin> poins = [
    Poin("Penghargaan"),
    Poin("Pelanggaran"),
  ];

  List<DropdownMenuItem> generateItems(List<Poin> poins) {
    List<DropdownMenuItem> items = [];
    for (var item in poins) {
      items.add(
        DropdownMenuItem(
          value: item,
          child: Text(item.typePoin),
        ),
      );
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back),
        ),
        backgroundColor: const Color(0xff53A4F5),
        title: const Text(
          "Ubah Data Master Poin",
          style: TextStyle(
            fontSize: 14,
          ),
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              'Kode Poin *',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              decoration: BoxDecoration(
                color: const Color(0xff92BAE9),
                borderRadius: BorderRadius.circular(12),
              ),
              child: TextField(
                readOnly: true,
                style: const TextStyle(fontSize: 14),
                onTap: () {},
                decoration: const InputDecoration(
                    hintText: 'P-003',
                    hintStyle: TextStyle(color: Colors.black),
                    alignLabelWithHint: true,
                    contentPadding: EdgeInsets.only(
                      left: 10,
                      right: 15,
                    ),
                    border: InputBorder.none),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              'Jenis Poin *',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              decoration: BoxDecoration(
                color: const Color(0xff92BAE9),
                borderRadius: BorderRadius.circular(12),
              ),
              child: TextField(
                readOnly: true,
                style: const TextStyle(fontSize: 14),
                onTap: () {},
                decoration: const InputDecoration(
                    hintText: 'Penghargaan',
                    hintStyle: TextStyle(color: Colors.black),
                    alignLabelWithHint: true,
                    contentPadding: EdgeInsets.only(
                      left: 10,
                      right: 15,
                    ),
                    border: InputBorder.none),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              'Nama Poin *',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              decoration: BoxDecoration(
                color: const Color(0xffE7F2FF),
                borderRadius: BorderRadius.circular(12),
              ),
              child: TextField(
                style: const TextStyle(fontSize: 14),
                onTap: () {},
                decoration: const InputDecoration(
                    hintText: 'Nama Poin',
                    hintStyle: TextStyle(fontSize: 14),
                    alignLabelWithHint: true,
                    contentPadding: EdgeInsets.only(
                      left: 10,
                      right: 15,
                    ),
                    border: InputBorder.none),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              'Poin *',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              decoration: BoxDecoration(
                color: const Color(0xffE7F2FF),
                borderRadius: BorderRadius.circular(12),
              ),
              child: TextField(
                style: const TextStyle(fontSize: 14),
                onTap: () {},
                decoration: const InputDecoration(
                    hintText: 'Contoh : 15',
                    hintStyle: TextStyle(fontSize: 14),
                    alignLabelWithHint: true,
                    contentPadding: EdgeInsets.only(
                      left: 10,
                      right: 15,
                    ),
                    border: InputBorder.none),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              'Keterangan',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 120,
              decoration: BoxDecoration(
                color: const Color(0xffE7F2FF),
                borderRadius: BorderRadius.circular(12),
              ),
              child: TextField(
                maxLines: 5,
                style: const TextStyle(fontSize: 14),
                onTap: () {},
                decoration: const InputDecoration(
                    hintText: 'Opsional',
                    hintStyle: TextStyle(fontSize: 14),
                    alignLabelWithHint: true,
                    contentPadding: EdgeInsets.only(
                      top: 10,
                      left: 10,
                      right: 15,
                    ),
                    border: InputBorder.none),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            const Text("*) Harus diisi"),
            const SizedBox(
              height: 35,
            ),
            InkWell(
              onTap: () {},
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 38,
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.circular(16),
                ),
                child: const Center(
                  child: Text(
                    'Tambah',
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.w600),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Poin {
  final String typePoin;
  Poin(this.typePoin);
}
