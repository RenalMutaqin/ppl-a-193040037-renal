import 'package:flutter/material.dart';
import 'package:perizinan_keluar_sekolah/master_poin/add_master_poin.dart';
import 'package:perizinan_keluar_sekolah/master_poin/card_master_poin.dart';

class HomeMasterPoin extends StatelessWidget {
  const HomeMasterPoin({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xff53A4F5),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back),
        ),
        title: const Text(
          "Data Master Poin",
          style: TextStyle(
            fontSize: 14,
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const AddMasterPoin(),
                ),
              );
            },
            icon: const Icon(Icons.add),
          ),
        ],
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(50),
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Container(
              height: 36,
              padding: const EdgeInsets.symmetric(
                horizontal: 15,
              ),
              decoration: BoxDecoration(
                color: const Color(0xffF0F0F0),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Stack(
                children: [
                  const Positioned(
                    top: 0,
                    bottom: 0,
                    child: Icon(
                      Icons.search,
                      color: Color(0xff53A4F5),
                    ),
                  ),
                  TextField(
                    style: const TextStyle(fontSize: 14),
                    onTap: () {},
                    decoration: const InputDecoration(
                        hintText: 'Search...',
                        hintStyle: TextStyle(fontSize: 14),
                        contentPadding: EdgeInsets.only(
                          top: 11,
                          bottom: 7,
                          left: 35,
                          right: 15,
                        ),
                        isDense: true,
                        border: InputBorder.none),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      body: ListView(
        padding: const EdgeInsets.all(15),
        children: const [
          CardMasterPoin(
            colorPoin: Color(0xff53A4F5),
            jenisPoin: 'Penghargaan',
            kodePoin: 'P-001',
            namaPoin: 'Juara Lomba Tingkat Nasional',
            nilaiPoin: '100',
          ),
          SizedBox(
            height: 20,
          ),
          CardMasterPoin(
            colorPoin: Color(0xffFF567A),
            jenisPoin: 'Pelanggaran',
            kodePoin: 'S-001',
            namaPoin: 'Dosa Besar',
            nilaiPoin: '- 100',
          ),
          SizedBox(
            height: 20,
          ),
          CardMasterPoin(
            colorPoin: Color(0xff53A4F5),
            jenisPoin: 'Penghargaan',
            kodePoin: 'P-002',
            namaPoin: 'Berpakaian Rapih',
            nilaiPoin: '50',
          ),
          SizedBox(
            height: 20,
          ),
          CardMasterPoin(
            colorPoin: Color(0xffFF567A),
            jenisPoin: 'Pelanggaran',
            kodePoin: 'S-002',
            namaPoin: 'Merokok',
            nilaiPoin: '- 50',
          ),
        ],
      ),
    );
  }
}
