import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:perizinan_keluar_sekolah/siswa/card_detail_poin_siswa.dart';

class DetailPoinSiswa extends StatefulWidget {
  const DetailPoinSiswa({super.key});

  @override
  State<DetailPoinSiswa> createState() => _DetailPoinSiswaState();
}

class _DetailPoinSiswaState extends State<DetailPoinSiswa> {
  Map<String, double> dataMap = {
    "Pidi Baiq": 80,
    "Budi Dalton": 20,
  };

  List<Color> colorList = [
    const Color(0xff53A4F5),
    const Color(0xffFF567A),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back),
        ),
        backgroundColor: const Color(0xff53A4F5),
        title: const Text(
          "Detail Poin Siswa",
          style: TextStyle(
            fontSize: 14,
          ),
        ),
      ),
      body: SingleChildScrollView(
        padding:
            const EdgeInsets.only(left: 15, right: 15, top: 20, bottom: 20),
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 20),
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 1,
                    blurRadius: 7,
                    offset: const Offset(0, 1),
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 15, top: 20, right: 15, bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          flex: 3,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text(
                                'NIPD',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                '193040009',
                                overflow: TextOverflow.ellipsis,
                              ),
                            ],
                          ),
                        ),
                        Flexible(
                          flex: 3,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text(
                                'Kelas',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text('X - RPL - A'),
                            ],
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    const Text(
                      'Nama Siswa',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    const Text(
                      'Muhammad Wildhan Kusumawardana',
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    PieChart(
                      dataMap: dataMap,
                      colorList: colorList,
                      chartRadius: MediaQuery.of(context).size.width / 2.5,
                      chartLegendSpacing: 15,
                      animationDuration: const Duration(seconds: 3),
                      chartValuesOptions: const ChartValuesOptions(
                          chartValueStyle: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.normal,
                              color: Colors.black),
                          showChartValues: true,
                          showChartValuesOutside: true,
                          showChartValuesInPercentage: true,
                          showChartValueBackground: false),
                      legendOptions: const LegendOptions(
                        showLegends: true,
                        legendShape: BoxShape.rectangle,
                        legendTextStyle: TextStyle(fontSize: 14),
                        legendPosition: LegendPosition.bottom,
                        showLegendsInRow: true,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 36,
              padding: const EdgeInsets.symmetric(
                horizontal: 15,
              ),
              decoration: BoxDecoration(
                color: const Color(0xffF0F0F0),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Stack(
                children: [
                  const Positioned(
                    top: 0,
                    bottom: 0,
                    child: Icon(
                      Icons.search,
                      color: Color(0xff53A4F5),
                    ),
                  ),
                  TextField(
                    style: const TextStyle(fontSize: 14),
                    onTap: () {},
                    decoration: const InputDecoration(
                        hintText: 'Search...',
                        hintStyle: TextStyle(fontSize: 14),
                        contentPadding: EdgeInsets.only(
                          top: 11,
                          bottom: 7,
                          left: 35,
                          right: 15,
                        ),
                        isDense: true,
                        border: InputBorder.none),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const CardDetailPoinSiswa(
              colorPoin: Color(0xff53A4F5),
              status: 'Penghargaan',
              kodePoin: 'P-001',
              namaPoin: 'Point Penghargaan 01',
              nilaiPoin: '100',
            ),
            const SizedBox(
              height: 20,
            ),
            const CardDetailPoinSiswa(
              colorPoin: Color(0xffFF567A),
              status: 'Pelanggaran',
              kodePoin: 'S-003',
              namaPoin: 'Dosa Besar',
              nilaiPoin: '- 1000',
            ),
          ],
        ),
      ),
    );
  }
}
