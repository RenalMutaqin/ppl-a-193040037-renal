import 'package:flutter/material.dart';

class KonfigurasiBatasPoin extends StatelessWidget {
  const KonfigurasiBatasPoin({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back),
        ),
        backgroundColor: const Color(0xff53A4F5),
        title: const Text(
          "Konfigurasi Batas Poin",
          style: TextStyle(
            fontSize: 14,
          ),
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              'Poin Batas SP1 *',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              decoration: BoxDecoration(
                color: const Color(0xffE7F2FF),
                borderRadius: BorderRadius.circular(12),
              ),
              child: TextField(
                style: const TextStyle(fontSize: 14),
                onTap: () {},
                decoration: const InputDecoration(
                    hintText: '100',
                    hintStyle: TextStyle(fontSize: 14, color: Colors.black),
                    alignLabelWithHint: true,
                    contentPadding: EdgeInsets.only(
                      left: 10,
                      right: 15,
                    ),
                    border: InputBorder.none),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              'Poin Batas SP2 *',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              decoration: BoxDecoration(
                color: const Color(0xffE7F2FF),
                borderRadius: BorderRadius.circular(12),
              ),
              child: TextField(
                style: const TextStyle(fontSize: 14),
                onTap: () {},
                decoration: const InputDecoration(
                    hintText: '250',
                    hintStyle: TextStyle(fontSize: 14, color: Colors.black),
                    alignLabelWithHint: true,
                    contentPadding: EdgeInsets.only(
                      left: 10,
                      right: 15,
                    ),
                    border: InputBorder.none),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              'Poin Batas SP3 *',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              decoration: BoxDecoration(
                color: const Color(0xffE7F2FF),
                borderRadius: BorderRadius.circular(12),
              ),
              child: TextField(
                style: const TextStyle(fontSize: 14),
                onTap: () {},
                decoration: const InputDecoration(
                    hintText: '500',
                    hintStyle: TextStyle(fontSize: 14, color: Colors.black),
                    alignLabelWithHint: true,
                    contentPadding: EdgeInsets.only(
                      left: 10,
                      right: 15,
                    ),
                    border: InputBorder.none),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text("*) Harus diisi"),
            const SizedBox(
              height: 20,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: const Color(0xff95C5FF),
                borderRadius: BorderRadius.circular(12),
              ),
              child: const Padding(
                padding:
                    EdgeInsets.only(left: 10, right: 10, top: 15, bottom: 15),
                child: Text(
                  'Ketika batas poin di set maka otomatis siswa yang melakukan pelanggaran tersebut telah masuk batas poin maka akan mendapatkan surat pelanggaran kepada orang tua',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 35,
            ),
            InkWell(
              onTap: () {},
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 38,
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.circular(16),
                ),
                child: const Center(
                  child: Text(
                    'Simpan',
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.w600),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
