import 'package:flutter/material.dart';
import 'package:perizinan_keluar_sekolah/verifikasi_poin/card_verifikasi_poin.dart';
import 'package:perizinan_keluar_sekolah/verifikasi_poin/detail_riwayat_verifikasi.dart';
import 'package:perizinan_keluar_sekolah/verifikasi_poin/detail_verifikasi_pengajuan.dart';

class HomeVerifikasiPoin extends StatelessWidget {
  const HomeVerifikasiPoin({super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xff53A4F5),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(Icons.arrow_back),
          ),
          title: const Text(
            "Verifikasi Poin Siswa",
            style: TextStyle(
              fontSize: 14,
            ),
          ),
          bottom: PreferredSize(
            preferredSize: const Size.fromHeight(100),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Container(
                    height: 36,
                    padding: const EdgeInsets.symmetric(
                      horizontal: 15,
                    ),
                    decoration: BoxDecoration(
                      color: const Color(0xffF0F0F0),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Stack(
                      children: [
                        const Positioned(
                          top: 0,
                          bottom: 0,
                          child: Icon(
                            Icons.search,
                            color: Color(0xff53A4F5),
                          ),
                        ),
                        TextField(
                          style: const TextStyle(fontSize: 14),
                          onTap: () {},
                          decoration: const InputDecoration(
                              hintText: 'Search...',
                              hintStyle: TextStyle(fontSize: 14),
                              contentPadding: EdgeInsets.only(
                                top: 11,
                                bottom: 7,
                                left: 35,
                                right: 15,
                              ),
                              isDense: true,
                              border: InputBorder.none),
                        ),
                      ],
                    ),
                  ),
                ),
                const TabBar(
                  indicatorColor: Color(0xffCACACA),
                  tabs: [
                    Tab(
                      text: 'Verifikasi Pengajuan',
                    ),
                    Tab(
                      text: 'Riwayat Verifikasi',
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        body: const TabBarView(
          children: [
            VerifikasiPengajuan(),
            RiwayatVerifikasi(),
          ],
        ),
      ),
    );
  }
}

class VerifikasiPengajuan extends StatelessWidget {
  const VerifikasiPengajuan({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.all(15),
      children: [
        InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const DetailVerifikasiPengajuan(),
              ),
            );
          },
          child: const CardVerifikasiPoin(
            colorStatus: Colors.orangeAccent,
            colorPoin: Color(0xff53A4F5),
            nipd: '193040009',
            statusTransaksi: 'Pending',
            kelas: 'X - RPL - A',
            namaSiswa: 'Muhammad Wildhan Kusumawardana',
            tanggalPengajuan: '2022-11-05 07:45:43',
            namaPengaju: 'Muhammad Ridwan Fuzan',
            nilaiPoin: '100',
            isPending: true,
          ),
        ),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }
}

class RiwayatVerifikasi extends StatelessWidget {
  const RiwayatVerifikasi({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.all(15),
      children: [
        InkWell(
          onTap: (() {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const DetailRiwayatVerifikasi(),
              ),
            );
          }),
          child: const CardVerifikasiPoin(
            colorStatus: Color(0xff53A4F5),
            colorPoin: Color(0xff53A4F5),
            nipd: '193040009',
            statusTransaksi: 'Diterima',
            kelas: 'X - RPL - A',
            namaSiswa: 'Muhammad Wildhan Kusumawardana',
            tanggalPengajuan: '2022-11-05 07:45:43',
            namaPengaju: 'Muhammad Ridwan Fuzan',
            namaPengesah: 'Muhammad Fauzan Ridwan',
            nilaiPoin: '100',
          ),
        ),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }
}
