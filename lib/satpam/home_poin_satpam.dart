import 'package:flutter/material.dart';
import 'package:perizinan_keluar_sekolah/siswa/card_poin_siswa.dart';

class Poin {
  final String typePoin;
  Poin(this.typePoin);
}

class HomePoinSatpam extends StatefulWidget {
  const HomePoinSatpam({super.key});

  @override
  State<HomePoinSatpam> createState() => _HomePoinSatpamState();
}

class _HomePoinSatpamState extends State<HomePoinSatpam> {
  Poin? selectedPoin;

  List<Poin> poins = [
    Poin("Pidi Baiq"),
    Poin("Budi Dalton"),
  ];

  List<DropdownMenuItem> generateItems(List<Poin> poins) {
    List<DropdownMenuItem> items = [];
    for (var item in poins) {
      items.add(
        DropdownMenuItem(
          value: item,
          child: Text(item.typePoin),
        ),
      );
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 255, 255, 255),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(100),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(15),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 36,
                  decoration: BoxDecoration(
                    color: const Color(0xffE7F2FF),
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                        hint: const Text(
                          "Guru Piket Hari Ini",
                          style: TextStyle(fontSize: 14),
                        ),
                        isExpanded: true,
                        value: selectedPoin,
                        items: generateItems(poins),
                        onChanged: (item) {
                          setState(() {
                            selectedPoin = item;
                          });
                        },
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
                child: Container(
                  height: 36,
                  padding: const EdgeInsets.symmetric(
                    horizontal: 15,
                  ),
                  decoration: BoxDecoration(
                    color: Color.fromARGB(255, 240, 240, 240),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Stack(
                    children: [
                      const Positioned(
                        top: 0,
                        bottom: 0,
                        child: Icon(
                          Icons.search,
                          color: Color(0xff53A4F5),
                        ),
                      ),
                      TextField(
                        style: const TextStyle(fontSize: 14),
                        onTap: () {},
                        decoration: const InputDecoration(
                            hintText: 'Search...',
                            hintStyle: TextStyle(fontSize: 14),
                            contentPadding: EdgeInsets.only(
                              top: 11,
                              bottom: 7,
                              left: 35,
                              right: 15,
                            ),
                            isDense: true,
                            border: InputBorder.none),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: ListView(
        padding: const EdgeInsets.all(15),
        children: [
          InkWell(
            onTap: () {},
            child: const CardPoinSiswa(
              nipd: '193040037',
              keperluan: 'Ke Dokter',
              namaSiswa: 'Renal Mutaqin',
              lamakeluar: '120 Menit',
            ),
          ),
          TextButton(
            style: TextButton.styleFrom(
                backgroundColor: Color.fromARGB(255, 0, 110, 255)),
            onPressed: () {},
            child: const Text(
              "Set Waktu Keluar",
              style: TextStyle(color: Color(0xfffffffff)),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          InkWell(
            onTap: () {},
            child: const CardPoinSiswa(
              nipd: '193040009',
              keperluan: 'Ke Dokter Gigi',
              namaSiswa: 'Amisha',
              lamakeluar: '175 Menit',
            ),
          ),
          TextButton(
            style: TextButton.styleFrom(
                backgroundColor: Color.fromARGB(255, 0, 110, 255)),
            onPressed: () {},
            child: const Text(
              "Set Waktu Keluar",
              style: TextStyle(color: Color(0xfffffffff)),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          InkWell(
            onTap: () {},
            child: const CardPoinSiswa(
              nipd: '193040025',
              keperluan: 'Bawa Laptop',
              namaSiswa: 'Sofyan Egi Lesmana',
              lamakeluar: '100 Menit',
            ),
          ),
          TextButton(
            style: TextButton.styleFrom(
                backgroundColor: Color.fromARGB(255, 0, 110, 255)),
            onPressed: () {},
            child: const Text(
              "Set Waktu Keluar",
              style: TextStyle(color: Color(0xfffffffff)),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}
