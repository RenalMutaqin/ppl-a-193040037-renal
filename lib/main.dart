import 'package:flutter/material.dart';
import 'package:perizinan_keluar_sekolah/main_menu.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Perizinan Keluar Sekolah',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const MainMenu());
  }
}
